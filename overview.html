<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" xmlns:xml="http://www.w3.org/XML/1998/namespace" version="-//W3C//DTD XHTML 1.1//EN"><head><title>Overview</title><meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
<meta name="generator" content="AsciiDoc 8.6.8"/>
<link rel="stylesheet" href="asciidoc/asciidoc.css" type="text/css"/>
<link rel="stylesheet" href="asciidoc/pygments.css" type="text/css"/>
<link rel="stylesheet" href="asciidoc/toc2.css" type="text/css"/>
<link rel="stylesheet" href="asciidoc/master.css" type="text/css"/>
<script type="text/javascript" src="asciidoc/asciidoc.js"></script>
<script type="text/javascript">
/**/
asciidoc.install(3);
/**/
</script><link href="asciidoc/jquery-ui-1.11.1.custom/jquery-ui.css" rel="stylesheet"/>
<script src="asciidoc/jquery-ui-1.11.1.custom/external/jquery/jquery.js"></script>
<script src="asciidoc/jquery-ui-1.11.1.custom/jquery-ui.js"></script>
<script src="asciidoc/hooks.js"></script>
<script src="local/asciidoc/hooks.js"></script>
</head>
<body class="article"><div id="header">
<h1>Overview</h1>
<div id="toc">
  <div id="toctitle">Table of Contents</div>
  <noscript><p><b>JavaScript must be enabled in your browser to display the table of contents.</b></p>
</noscript>

</div>
</div><div id="content">
<div id="preamble">
<div class="sectionbody">
<div id="sitemap-container">
<iframe src="sitemap-content.html"></iframe>

</div>
<div class="paragraph"><p><a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> is a shared, structured store for built software modules and <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> is a
tool which helps you package software <a class="glossary-link" href="glossary.html#gloss-module" shape="rect">module</a>s and transfer them to and from such a store.
(Gradle also has many other features which are not detailed in this guide.)</p>
</div>

<div class="paragraph"><p>The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> is a set of plugins for Gradle which extend it for</p></div>

<div class="ulist"><ul><li>
<p>
developing modules which contain a large number of files when built (such as C++ libraries or
web applications);
</p>
</li><li>
<p>
working with multiple source code repositories;
</p>
</li><li>
<p>
delivering modules plus dependencies to developers who do not have access to Artifactory; and
</p>
</li><li>
<p>
working under Windows.
</p>
</li></ul>
</div>

<div class="paragraph"><p>This section gives a short introduction to the main concepts and features.  The following diagram
shows how they fit in with other development tools.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_artifactory_and_gradle_in_context.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 1: Artifactory and Gradle in context</div>
</div>
<div class="admonitionblock admonitiontype-todo">
<table><tr><td class="icon" rowspan="1" colspan="1">
<div class="title">TO DO</div>
</td><td class="content" rowspan="1" colspan="1">Link to pages with more detail.</td></tr>
</table>

</div>
</div>
</div>
<div class="sect1">
<h2 id="_gradle_module_concepts">Gradle Module Concepts</h2>
<div class="sectionbody">
<div class="paragraph"><p>In <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> terms, a published <a class="glossary-link" href="glossary.html#gloss-module" shape="rect">module</a> is a collection of files (which Gradle
calls <a class="glossary-link" href="glossary.html#gloss-artifact" shape="rect">artifact</a>s), plus metadata which identifies the module and describes how it depends
on other modules.  Modules may be published by many groups in different organisations, so a
module identifier has three parts: <em>group</em> (also called <em>organisation</em>), <em>name</em>, and <em>version</em>.
As an easy way to avoid conflicts, the group string often includes the reverse domain name of the
organisation, in the Java style; for example, <code>com.mycorp.teamA</code>.</p></div>

<div class="paragraph"><p>Modules are stored in <a class="glossary-link" href="glossary.html#gloss-repositories" shape="rect">repositories</a>, which are usually on a shared server but can also be
on the local file system.  When a developer uses Gradle to download a module, the
dependencies for that module are automatically also downloaded.  Gradle will check that
all versions are consistent, even if the same dependency is requested by several different
libraries.</p></div>

<div class="paragraph"><p>A module may contain several artifacts for different aspects of the same software component, if
they are needed in different contexts.  For example, one part may be for normal runtime use and
another for debugging; or it might be a communication library with separate client and server
parts.  In a <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> module these parts are called <a class="glossary-link" href="glossary.html#gloss-configuration" shape="rect">configuration</a>s.  One
configuration in a module can extend (include) other configurations to share artifacts.  For
example, the client and server parts of a communication library may need to share some definitions.</p></div>

<div class="paragraph"><p>Each part may need different sets of other modules; therefore, each configuration has its own
collection of dependencies.  When we say &quot;the dependencies of a module&quot; we really mean &quot;the
dependencies of the configurations of a module&quot;.</p></div>

<div class="paragraph"><p>The relationships between these concepts can be viewed as follows.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_module_concepts.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 2: Gradle module concept relationships</div>
</div>
<div class="paragraph"><p>Ignoring the detail of configurations, the relationships between some modules could be viewed as
follows, where an arrow means &quot;depends on&quot;.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_modules_1.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 3: Basic view of module dependencies</div>
</div>
<div class="paragraph"><p>Suppose some team creates a new module N which uses A and C, giving the dependency graph below.
Gradle understands that B is also needed, and will check that the version of C is
consistent.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_modules_2.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 4: More complex module dependencies</div>
</div>
<div class="paragraph"><p>Now, suppose these modules each have configurations <code>compile</code> and <code>runtime</code>.  Then A would
typically declare that its <code>compile</code> configuration depends on <code>compile</code> from B, and that its
<code>runtime</code> configuration depends on B’s <code>runtime</code> configuration.  This means that another module such
as N, which declares a dependency on the <code>runtime</code> configuration of A, will get the <code>runtime</code> files
for B as well as A.</p></div>

<div class="paragraph"><p>Suppose N uses C as a static library, but uses A as a plugin so it only needs <code>runtime</code> files
for A and not header and linker files from the <code>compile</code> configuration.  Then diagram below
highlights in blue the configurations which will be fetched for the configurations of N.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_config_deps_1.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 5: Dependencies between configurations</div>
</div>
<div class="paragraph"><p>More details on extending configurations, and mapping sets of related configurations between modules,
can be found in the <a href="workflows.html#_configurations" shape="rect">Configurations</a> section of the link:workflows.html page.</p></div>

</div>
</div>
<div class="sect1">
<h2 id="_artifactory">Artifactory</h2>
<div class="sectionbody">
<div class="paragraph"><p><a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> is a web service for storing modules which are published using tools such as
<a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a>.  The simplest view of it is &quot;version control for binaries&quot; or &quot;FTP / file share with
history&quot;.  However, it has more features and some standards to follow, described in this section.</p>
</div>

<div class="sect2">
<h3 id="_repositories">Repositories</h3>
<div class="paragraph"><p>An <a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> server instance does not allow users to store files in any structure they
choose.  It stores modules in named collections, called <a class="glossary-link" href="glossary.html#gloss-repositories" shape="rect">repositories</a>.  These can only be
created by server administrators.  Within each repository (or &quot;repo&quot;), the files for each module are
in a three-level folder structure: group, then name, then version.</p></div>

<div class="paragraph"><p><a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> accesses modules by URL, usually of the format <code>https://server.example-corp.com/artifactory/<em>repository</em>/<em>group</em>/<em>name</em>/<em>version</em>/…</code>.  Within the
Artifactory web UI, the contents can be seen on the &quot;Artifacts&quot; tab.  (Note that the web UI
does not exactly match the view used by tools like Gradle, as described below.)</p>
</div>

<div class="paragraph"><p>There are three kinds of repository on a server: local, remote, and virtual.  Normally only server
administrators need to deal with remote repositories directly.</p></div>

<div class="dlist"><dl><dt class="hdlist1">
Local
</dt><dd>
<p>
These contain artifacts whose contents are managed by that server.  The word &quot;local&quot; does
<strong>not</strong> mean &quot;only visible within one organisation&quot; — visibility depends on permissions, and
visibility to other organisations depends on network configuration.  They are usually named
<code><em>myrepo</em>-local</code>
, and both Gradle and the web UI refer to them in this way.
</p>
</dd><dt class="hdlist1">
Remote
</dt><dd>
<p>
These contains local copies of artifacts automatically cached from a repo on another
server.  Cached files may be downloaded on demand or in advance, and may be automatically deleted,
depending on server setup.  In the &quot;Artifacts&quot; tab of the web UI these appear as <code><em>myrepo</em>-cache</code>
 in
the &quot;Tree Browser&quot; and show only the files which have already been cached.  When using
Gradle they are referenced as just <code><em>myrepo</em></code>
, and any attempt to GET a file which is not
already cached will cause it to be cached.  Both views can be found under the &quot;Simple Browser&quot;
section of the web UI.  Normally you will not refer to a remote repository directly, but will use a
virtual repo which includes that remote.
</p>
</dd><dt class="hdlist1">
Virtual
</dt><dd>
<p>
These point to one or more other repos, using a list configured by admins.  Requests for
artifacts from these repos will search the list in order.  Like remote repos, virtual repos are also
named as just <code><em>myrepo</em></code>
.  These are only shown in the &quot;Simple Browser&quot;, not in the &quot;Tree Browser&quot;.
For a given folder or file in the &quot;Tree Browser&quot; you can find which virtual repos include it by
looking in the &quot;Virtual Repository Associations&quot; section on the right.
</p>
</dd></dl>
</div>

<div class="paragraph"><p>Usually a server admin will set up one or more virtual repos for a team which includes all the local
and remote repos they need.  This is convenient for the team, because they only need to list one
repo in their <code>build.gradle</code> file.  It is also important for admins, if they need to change
repository configuration in future, for example, to change permissions or backup processes.  When
they make such changes they can also change the virtual repo lists, so that all requests to the
virtual repo work as before.</p></div>

<div class="admonitionblock admonitiontype-important">
<table><tr><td class="icon" rowspan="1" colspan="1">
<div class="title">Important</div>
</td><td class="content" rowspan="1" colspan="1">
<div class="paragraph"><p>You can think of virtual repos like &quot;interfaces&quot;, and local and remote repos like &quot;implementation&quot;.
You should always set up your <code>build.gradle</code> scripts to</p></div>

<div class="ulist"><ul><li>
<p>
get dependencies from a virtual repo (so that admins can re-arrange the other repos); and
</p>
</li><li>
<p>
publish to a local repo (because you can’t publish to the other two).
</p>
</li></ul>
</div>

</td></tr>
</table>

</div>
</div>
<div class="sect2">
<h3 id="_access_control">Access Control</h3>
<div class="paragraph"><p><a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> has access control, so different users may have different permissions to read
or modify a repository.  The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> includes a feature to keep a developer’s
Artifactory password securely in the <a class="glossary-link" href="glossary.html#gloss-credential-manager" shape="rect">Windows Credential Manager</a>, so that it can log in
automatically without storing passwords in build scripts.</p>
</div>

</div>
<div class="sect2">
<h3 id="_module_version_lifetimes">Module Version Lifetimes</h3>
<div class="paragraph"><p>An important question which <a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> cannot fully answer is, after a version of a
module is published, when can it be deleted?  Official release versions may need to be kept for
many years but often teams also make temporary versions, and delete them to save disk space.</p></div>

<div class="paragraph"><p>A common approach is to put long-term and temporary releases in different repositories:
<code><em>something</em>-release-local</code>
 for &quot;permanent&quot; versions, and <code><em>something</em>-integration-local</code>
 for
temporary ones.  The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> provides a plugin for deleting older versions of modules
using date ranges and other rules, and it allows different rules for each repository.  Typically
this plugin would be run periodically using a tool like <a class="glossary-link" href="glossary.html#gloss-jenkins" shape="rect">Jenkins</a>.</p></div>

</div>
<div class="sect2">
<h3 id="_promotion">Promotion</h3>
<div class="paragraph"><p>A common development process called <em>continuous integration</em> involves building and testing a module
regularly, for example, any time a new source code change is committed.  When a build passes all
tests, it can be used as a release candidate.</p></div>

<div class="paragraph"><p>When a repository server like <a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> is involved, it is a good idea to publish each
build to a temporary repository.  This tests the publish process, and also allows the builds to
be downloaded from Artifactory for manual testing.  To release a successful build, you
could re-run the publish process targeting a repository which will store the module permanently.
However, Artifactory provides a quick way to copy or move a module to another repository on
the same server.</p></div>

</div>
</div>
</div>
<div class="sect1">
<h2 id="_gradle_and_the_holy_gradle">Gradle and the Holy Gradle</h2>
<div class="sectionbody">
<div class="paragraph"><p>The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> provides a way to publish and use pre-built and pre-packaged software
<a class="glossary-link" href="glossary.html#gloss-module" shape="rect">module</a>s produced with tools like C++, HTML, and JavaScript.  These tools do not provide
their own module packaging system and their output form can have a large number of files.  (In
contrast, Java and .NET have their own packaging systems, and only a small number of output files
per module).  A packaged <a class="glossary-link" href="glossary.html#gloss-module" shape="rect">module</a> may be a library, web component, build tool, end-user
application, test data set, or any other collection of files which are useful for developing software components.</p></div>

<div class="sect2">
<h3 id="_basic_modular_development_process">Basic Modular Development Process</h3>
<div class="paragraph"><p>The basic approach for developing with <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> is as follows.</p></div>

<div class="ulist"><ul><li>
<p>
Write a <code>build.gradle</code> file for your module (call it A) which lists
</p>
<div class="ulist"><ul><li>
<p>
the configurations of A;
</p>
</li><li>
<p>
the modules needed by each configuration (its <a class="glossary-link" href="glossary.html#gloss-dependencies" shape="rect">dependencies</a>), each with its own version
(for example, B:1.0 and C:1.5);
</p>
</li><li>
<p>
the locations for downloading those dependencies;
</p>
</li><li>
<p>
the artifacts to publish for each configuration of A;
</p>
</li><li>
<p>
the location to publish A.
</p>
</li></ul>
</div>

</li><li>
<p>
Run Gradle to download the dependencies.
</p>
</li><li>
<p>
Build and test A.
</p>
</li><li>
<p>
Run Gradle to publish A.
</p>
</li></ul>
</div>

<div class="paragraph"><p>The downloading and publishing of modules in the above example could look like this, where an arrow
means &quot;file transfer&quot;.  The group parts of the module IDs are omitted for simplicity.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_basic_gradle_use.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 6: Basic view of using Gradle</div>
</div>
<div class="paragraph"><p>The transferred files also include a metadata file describing the module.  For the
<a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> this is an <a class="glossary-link" href="glossary.html#gloss-ivy-xml" shape="rect">ivy.xml</a> file.</p></div>

</div>
<div class="sect2">
<h3 id="_holy_gradle_modules">Holy Gradle Modules</h3>
<div class="paragraph"><p>The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> plugins add several features to processes of fetching and publishing modules.</p></div>

<div class="sect3">
<h4 id="_packed_dependencies">Packed Dependencies</h4>
<div class="paragraph"><p>The artifacts of Java-based modules are often only one or two JAR files.  To build a module, the
file names of these JAR dependencies are simply passed to the Java compiler on the command line.</p></div>

<div class="paragraph"><p>For languages like C++, each module may provide many files — header files, static and dynamic
libraries, resources, and so on.  Also, some of these files may have variants such as for &quot;release&quot;
and &quot;debug&quot; mode, or for 32-bit and 64-bit platforms.  To build such a module, the file names
from its dependencies are often too many to pass on a command line, and compilers expect to get a
list of folders to search for header files etc.</p></div>

<div class="paragraph"><p>The basic extension which the <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a>'s <a href="plugin-intrepid.html" shape="rect">intrepid-plugin</a> provides is to
collect files into one or more ZIP files (<a class="glossary-link" href="glossary.html#gloss-package" shape="rect">package</a>s) when publishing, and unzip those files
(into your <a class="glossary-link" href="glossary.html#gloss-gradle-user-home" shape="rect">Gradle user home</a>) when dependencies are downloaded.  All packages are unpacked
into the same folder, so that folder will contain the union of all those files.  It also extends the
<a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> dependency syntax to specify a relative folder path for each dependency, and the
unzipped files appear at that location in your <a class="glossary-link" href="glossary.html#gloss-workspace" shape="rect">workspace</a> using <a class="glossary-link" href="glossary.html#gloss-link" shape="rect">link</a>s.  The
compiler can then be configured to look for the dependency files at those locations.</p></div>

</div>
<div class="sect3">
<h4 id="_source_dependencies">Source Dependencies</h4>
<div class="paragraph"><p>In <a class="glossary-link" href="glossary.html#gloss-subversion" shape="rect">Subversion</a> and some other source control systems, a very simple way to handle
dependencies is to add them as &quot;externals&quot;.  This has many disadvantages — for example, there is
no easy way use the same external at more than one place in the graph, as in the previous example
where module C is used by both A and N.  However, it is useful for combining source code from
multiple repositories, for cases where several components are always built together, which is not
well supported by some other systems such as <a class="glossary-link" href="glossary.html#gloss-mercurial" shape="rect">Mercurial</a>.</p></div>

<div class="paragraph"><p>The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> adds the notion of &quot;source dependencies&quot; to <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a>.  &quot;Downloading&quot;
a source dependency will check it out from source control.  When the original module is published,
its source dependencies will also be published, as separate modules.  The metadata will be
automatically filled in to describe the connections between these modules.</p></div>

</div>
<div class="sect3">
<h4 id="_source_reference_information">Source Reference Information</h4>
<div class="paragraph"><p>In the Java world, binary modules often have a matching source module, containing the same version
of the source code, for example for debugging use.  Java IDEs are often built to automatically
download source published in this way.  For C++ applications on Windows, the Visual Studio IDE
instead downloads source from a Microsoft Symbol Server.  Neither the <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> nor
<a class="glossary-link" href="glossary.html#gloss-artifactory" shape="rect">Artifactory</a> include special support for this.  However, the Holy Gradle automatically
adds a <code>build_info</code> folder to each module, which has a reference to the source code location and
revision.</p></div>

</div>
</div>
<div class="sect2">
<h3 id="_holy_gradle_plugins">Holy Gradle Plugins</h3>
<div class="paragraph"><p>The <a class="glossary-link" href="glossary.html#gloss-holygradle" shape="rect">Holy Gradle</a> is implemented as a set of <a class="glossary-link" href="glossary.html#gloss-gradle" shape="rect">Gradle</a> plugins, plus a custom distribution
of Gradle (for bootstrapping and debugging the plugins).  The section
<a href="first_steps.html#_minimal_build_file" shape="rect">A Minimal Holy Gradle Build File</a> shows how you include these plugins in your build file.</p></div>

<div class="paragraph"><p>DONE: <a href="https://bitbucket.org/nm2501/holy-gradle-plugins/wiki/Home" shape="rect">https://bitbucket.org/nm2501/holy-gradle-plugins/wiki/Home</a></p></div>

<div class="paragraph"><p>The usage relationship between the plugins, the custom distribution, and some other related tools,
is as follows.</p></div>

<div class="imageblock">
<div class="content">
<object data="overview_holygradle_components.svg" type="image/svg+xml"></object>

</div>
<div class="title">Figure 7: Holy Gradle component dependencies</div>
</div>
<div class="paragraph"><p>More detail on each plugin can be found on the <a href="plugins.html" shape="rect">Holy Gradle Software Components</a> page.  The original
<a href="requirements.html" shape="rect">Requirements</a> are also part of this documentation.</p></div>

<hr></hr>

<div class="admonitionblock admonitiontype-todo">
<table><tr><td class="icon" rowspan="1" colspan="1">
<div class="title">TO DO</div>
</td><td class="content" rowspan="1" colspan="1">Link to doc on how to do more complex cases with dependencies which also need to be promoted.</td></tr>
</table>

</div>
</div>
</div>
</div>
</div><div id="footnotes"><hr></hr>
</div>
<div id="footer">
<div id="footer-text">
Built from 42e14ffb96b6 at 2017-12-07 13:30:48 UTC
</div>
</div></body>
</html>
